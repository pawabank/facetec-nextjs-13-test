Steps to recreate the crash during the id scan:

1. (optional) Change FaceTec config in `src/app/face/config.ts`
2. Run the server

```bash
yarn
yarn build
yarn start
```

3. Open http://localhost:3000 with your browser
4. Click the "Test FaceTec + Next 13" link
5. Wait for the SDK to initialize and launch
6. Scan your ID
7. Wait for the upload to finish successfully
8. There is a blank page instead of the data confirmation page and the following error is visible in the console:
```
Uncaught TypeError: Failed to execute 'getComputedStyle' on 'Window': parameter 1 is not of type 'Element'.
    at t.<computed>.<computed> [as setupOCRConfirmationScreenWithOCRData] (909.d6aac0999b4608aa.js:1:790207)
    at <computed> [as showOCRResults] (909.d6aac0999b4608aa.js:1:812129)
    at <computed> [as ContinueToOCRResults] (909.d6aac0999b4608aa.js:1:813008)
    at 909.d6aac0999b4608aa.js:1:816697
    at 909.d6aac0999b4608aa.js:1:769207
    at 909.d6aac0999b4608aa.js:1:1147454
t.<computed>.<computed> @ 909.d6aac0999b4608aa.js:1
<computed> @ 909.d6aac0999b4608aa.js:1
<computed> @ 909.d6aac0999b4608aa.js:1
...
```
The top of the stack trace is the following line:
```js
R[v(16868)][v(17306)] = getComputedStyle([][0])[v(17306)]
```
Since `[][0]` resolves to `undefined`, `getComputedStyle` throws an error.
