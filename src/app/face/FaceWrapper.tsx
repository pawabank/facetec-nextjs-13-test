'use client';
import React from "react";
import dynamic from "next/dynamic";

export const FaceWrapper = dynamic(
    () => import('./Face'),
    { loading: () => null, ssr: false }
)
