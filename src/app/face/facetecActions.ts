import type { FaceTecIDScanResult } from '../../../public/core-sdk/FaceTecSDK.js/FaceTecPublicApi';
import {Observable} from '../utils/observable';
import {deviceKey, encryptionKey} from './config';
import * as facetecApi from "./facetecApi";
import {FaceTecSDK} from "../../../public/core-sdk/FaceTecSDK.js/FaceTecSDK";
import {
    FaceTecIDScanProcessor,
    FaceTecIDScanResultCallback
} from "../../../public/core-sdk/FaceTecSDK.js/FaceTecPublicApi";
import {idScanOnly} from "./facetecApi";

export const initFaceTec = async (): Promise<boolean> => {
    if (FaceTecSDK.getStatus() === FaceTecSDK.FaceTecSDKStatus.Initialized) {
        return true;
    }
    FaceTecSDK.setImagesDirectory('/core-sdk/FaceTec_images');
    FaceTecSDK.setResourceDirectory('/core-sdk/FaceTecSDK.js/resources');
    return new Promise<boolean>(
        resolve => FaceTecSDK.initializeInDevelopmentMode(deviceKey, encryptionKey, resolve)
    );
};

export const getFaceTecSessionToken = async () => {
    const userAgent = FaceTecSDK.createFaceTecAPIUserAgentString('');
    return facetecApi.getSession(userAgent).then(response => response.json()).then(json => json.sessionToken);
};

export type IdScanResult = {
    idScanResult: FaceTecIDScanResult;
    idScanResultCallback: FaceTecIDScanResultCallback;
};

const startIdScan = (sessionToken: string, observable: Observable<IdScanResult>) => {
    const callbacks: FaceTecIDScanProcessor = {
        processIDScanResultWhileFaceTecSDKWaits: (idScanResult: FaceTecIDScanResult, idScanResultCallback: FaceTecIDScanResultCallback) => {
            console.log('processSessionResultWhileFaceTecSDKWaits');
            observable.nextValue({idScanResult, idScanResultCallback});
        },
        onFaceTecSDKCompletelyDone: () => {
            /* do nothing */
            console.log('onFaceTecSDKCompletelyDone');
        }
    };
    new FaceTecSDK.FaceTecSession(callbacks, sessionToken);
};

export const completeIdScanWithRetries = async (sessionToken: string): Promise<string> => {
    const sessionObservable = new Observable<IdScanResult>();
    startIdScan(sessionToken, sessionObservable);
    let tries = 1;
    const maxTries = 3;
    while (tries <= maxTries) {
        const {idScanResult, idScanResultCallback} = await sessionObservable.waitForNext();
        if (idScanResult.status !== FaceTecSDK.FaceTecIDScanStatus.Success) {
            idScanResultCallback.cancel();
            return 'failed';
        }
        const userAgent = FaceTecSDK.createFaceTecAPIUserAgentString(idScanResult.sessionId as string);
        const {success, isCompletelyDone, scanResultBlob, documentData} = await idScanOnly(
          {idScan: idScanResult.idScan, idScanFrontImage: idScanResult.frontImages[0], idScanBackImage: idScanResult.backImages[0]},
          userAgent,
          idScanResultCallback.uploadProgress,
        );
        if (isCompletelyDone) {
            idScanResultCallback.proceedToNextStep(scanResultBlob);
            return 'succeeded';
        } else if (success) {
            idScanResultCallback.proceedToNextStep(scanResultBlob);
        } else {
            // tries += 1;
            // if (tries > maxTries) {
            //     idScanResultCallback.cancel();
            // }
            idScanResultCallback.proceedToNextStep(scanResultBlob);
        }
    }
    return 'failed';
}
