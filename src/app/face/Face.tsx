'use client';
import React, {useEffect, useState} from "react";
import {completeIdScanWithRetries, getFaceTecSessionToken, initFaceTec} from "./facetecActions";

export default function Face() {

    const [status, setStatus] = useState('initial');
    const [sessionToken, setSessionToken] = useState('');

    useEffect(() => {
        if (status !== 'initial') return;
        setStatus('initializing');
        initFaceTec().then((initialized: boolean) => {
            setStatus(initialized ? 'initialized' : 'init failed');
        });
    }, [status]);

    useEffect(() => {
        if (status !== 'initialized') return;
        setStatus('starting session');
        getFaceTecSessionToken().then((sessionToken: string) => {
            setSessionToken(sessionToken);
            console.log('sessionToken', sessionToken);
            setStatus('session started');
        }).catch((e: any) => {
            setStatus('session failed to start');
            console.log(e);
        });
    }, [status]);

    useEffect(() => {
        if (status !== 'session started') return;
        setStatus('running');
        completeIdScanWithRetries(sessionToken)
            .then((status: string) => setStatus(status))
            .catch((e: any) => {
                setStatus('failed');
                console.log(e);
            });
    }, [status, sessionToken])


    return (
        <div className="flex flex-col space-5 items-center justify-center">
            <p className="text-xl" >{status}</p>
            {status === 'succeeded' && <button className="border border-white" onClick={() => setStatus('initialized')}>RESTART</button>}
        </div>
    );
}
