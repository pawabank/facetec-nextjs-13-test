export default function Home() {
  return (
    <main className="flex min-h-screen flex-row p-24">
        <span>---&gt;</span>
        <a
          href="/face"
          className="underline mx-2"
        >
            Test FaceTec + Next 13
        </a>
        <span>&lt;---</span>
    </main>
  )
}
