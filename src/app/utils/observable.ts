export class Observable<V> {
  private waitingQueue: { resolve: (v: V) => void; reject: (e: any) => void }[] = [];

  private resultsQueue: { value?: V; error?: any }[] = [];

  nextValue(value: V) {
    if (this.waitingQueue.length > 0) {
      const promiseResolvers = this.waitingQueue.shift();
      promiseResolvers!.resolve(value);
    } else {
      this.resultsQueue.push({ value });
    }
  }

  nextError(error: any) {
    if (this.waitingQueue.length > 0) {
      const promiseResolvers = this.waitingQueue.shift();
      promiseResolvers!.reject(error);
    } else {
      this.resultsQueue.push({ error });
    }
  }

  async waitForNext(): Promise<V> {
    if (this.resultsQueue.length > 0) {
      const result = this.resultsQueue.shift();
      if (result!.error) throw result!.error;
      return result!.value as V;
    }
    return new Promise<V>((resolve, reject) => {
      this.waitingQueue.push({ resolve, reject });
    });
  }
}
