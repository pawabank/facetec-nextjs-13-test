/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  // swc minifier breaks FaceTec SDK
  swcMinify: false
}

module.exports = nextConfig
